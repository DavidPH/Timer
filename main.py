import pygame as pg
import sys
import time
from datetime import timedelta
import _thread
import configparser
import os
import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox
import subprocess


def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)


pg.init()
pg.font.init()
pg.mixer.init()
b_img = [pg.image.load(resource_path("b.png")), pg.image.load(resource_path("b_pressed.png"))]
s_b_img = [pg.image.load(resource_path("s_b.png")), pg.image.load(resource_path("s_b_pressed.png"))]
mute_img = [pg.image.load(resource_path("sound.png")), pg.image.load(resource_path("mute.png"))]
info_img = [pg.image.load(resource_path("info_b.png")), pg.image.load(resource_path("info_b_pressed.png"))]
settings_img = [pg.image.load(resource_path("settings.png")), pg.image.load(resource_path("settings_pressed.png"))]
display_width = 600
display_height = 500
font = pg.font.SysFont("Comic Sans MS", 35, True)
medium_font = pg.font.SysFont("Comic Sans MS", 28, True)
small_font = pg.font.SysFont("Comic Sans MS", 20, True)
clock = pg.time.Clock()

default_time = '00:14:00'
default_shutdown_time = "00:00:00"
alert_path = resource_path('chimes.wav').replace('\\', '/')
alert = pg.mixer.Sound(alert_path)
timer_durations = [4, 16, 8]

muted = False

white = (239, 252, 250)
black = (0, 0, 0)
green = (153, 255, 147)
red = (230, 100, 100)
dark_green = (29, 132, 1)

mainDisplay = pg.display.set_mode((display_width, display_height))
pg.display.set_caption("Timer")
pg.display.set_icon(pg.image.load(resource_path("logo.png")))


class Button:

    def __init__(self, w, h, pos, text, img, toggle_button=False):
        self.pos = pos
        self.w = w
        self.h = h
        self.text = font.render(text, True, black)
        self.text_rect = self.text.get_rect()
        self.text_rect.center = (pos[0] + w / 2, pos[1] + h / 2)
        self.box = pg.Surface((w, h))
        self.rect = self.box.get_rect()
        self.rect.topleft = self.pos
        self.image = img

        self.active = False
        self.toggle_button = toggle_button
        self.toggle = False

    def draw(self):
        if self.active:
            # self.box.fill(green)
            img = self.image[1]
        else:
            img = self.image[0]
            # self.box.fill(dark_green)

        # gameDisplay.blit(self.box, self.pos)
        mainDisplay.blit(img, self.pos)
        mainDisplay.blit(self.text, self.text_rect)

    def click(self, event):
        if event.type == pg.MOUSEBUTTONDOWN:
            mouse = pg.mouse.get_pos()
            if self.rect.collidepoint(mouse):
                self.active = not self.active
                return True
        if event.type == pg.MOUSEBUTTONUP and not self.toggle_button:
            self.active = False
        return False

    def hover(self):
        if not self.toggle_button:
            mouse = pg.mouse.get_pos()
            if self.rect.collidepoint(mouse):
                self.active = True
                return True
            self.active = False
            return False

    def update_text(self, txt):
        self.text = font.render(txt, True, black)
        self.text_rect = self.text.get_rect()
        self.text_rect.center = (self.pos[0] + self.w / 2, self.pos[1] + self.h / 2)


class Timer:
    no_of_timers = 0
    instances = []

    def __init__(self, seconds):
        try:
            self.current_time = round(time.time())
            self.end_time = self.current_time + seconds
            self.seconds = seconds
            # _thread.start_new_thread(self.timer, ())
            Timer.no_of_timers += 1
            self.ID = Timer.no_of_timers
            self.rect = pg.Rect(0, 0, 150, 30)
            self.draw = True
            self.dead = False
            Timer.instances.append(self)
            instance = self.dead_timer()
            if instance:
                instance.draw = False
                self.ID = instance.ID
                self.rect = instance.rect
                Timer.no_of_timers -= 1
        except:
            pass

    def dead_timer(self):
        for instance in Timer.instances:
            if instance.dead and instance.draw:
                return instance
        return False

    def timer(self):

        if self.current_time != self.end_time and not self.dead:
            self.current_time = round(time.time())
            self.seconds = self.end_time - self.current_time
        if self.seconds <= 0:
            if not self.dead:
                alarm(timer_durations[0])
            self.seconds = 0
            self.dead = True

    def draw_time(self):
        if self.draw:
            self.timer()
            multiplier = self.ID
            if multiplier > 9:
                multiplier = multiplier - 9
                x_offset = 160
            else:
                x_offset = 0
            if multiplier == 1:
                y_offset = 55
            else:
                y_offset = 55

            if self.seconds <= 0:
                color = red
            else:
                color = dark_green

            m, s = divmod(self.seconds, 60)
            h, m = divmod(m, 60)
            text = small_font.render("{}:{}:{}".format(h, m, s), True, color)

            self.rect = pg.Rect(x_offset, y_offset * multiplier - 30, 150, 30)
            text_rect = [x_offset + 75 // 2, y_offset * multiplier - 30, 150, 30]
            pg.draw.rect(mainDisplay, black, self.rect, 2)
            mainDisplay.blit(text, text_rect)

    def clicked(self):
        mouse = pg.mouse.get_pos()
        if self.rect.collidepoint(mouse):
            self.seconds = 0
            self.dead = True


class ImperialTimer:

    def __init__(self, x, y):
        self.rect = pg.Rect(x, y, 150, 30)
        self.text_rect = [x + 75 // 2, y, 150, 30]
        self.label_rect = [x + 4, y - 35, 150, 30]
        self.imperial_times = [i for i in range(1, 25) if i % 3 == 0]
        self.sound_alarm = False
        self.alarm_count = 0

    def draw_time(self):
        nearest_time = 0
        color = black
        t = [time.gmtime()[3], time.gmtime()[4], time.gmtime()[5]]
        # t = [datetime.now().time().hour, datetime.now().time().minute, datetime.now().time().second] # for testing
        for times in self.imperial_times:
            if t[0] < times and times - t[0] <= 3:
                nearest_time = times
        t1 = timedelta(hours=t[0], minutes=t[1], seconds=t[2])
        t2 = timedelta(hours=nearest_time, minutes=0, seconds=0)
        left = t2 - t1
        if left.total_seconds() < 660:
            color = red
            if self.alarm_count == 0:
                alarm(timer_durations[1])
                self.alarm_count += 1

        else:
            self.alarm_count = 0
        if left.total_seconds() <= 1 and not self.sound_alarm:
            self.sound_alarm = True
            alarm(timer_durations[2])
        if left.total_seconds() >= 2:
            self.sound_alarm = False
        time_text = small_font.render(str(left), True, color)
        label_text = small_font.render("Imperial Reset", True, color)
        pg.draw.rect(mainDisplay, black, self.rect, 2)
        mainDisplay.blit(time_text, self.text_rect)
        mainDisplay.blit(label_text, self.label_rect)


class Entry:
    def __init__(self, w, h, pos, surface):
        self.pos = pos
        self.rect = pg.Rect(pos[0], pos[1], w, h)
        self.text_rect = pg.Rect(pos[0] + 10, pos[1], w, h)
        self.active = False
        self.text = default_time
        self.inactive_text_color = black
        self.active_text_color = dark_green
        self.current_color = self.inactive_text_color
        self.box_color = black
        self.index = 0
        self.count = 0
        self.surface = surface
        self.draw_cursor = True

    def event_handler(self, event):
        if event.type == pg.MOUSEBUTTONDOWN:
            mouse = pg.mouse.get_pos()
            if self.rect.collidepoint(mouse):
                self.active = not self.active
                self.text = "00:00:00"
            else:
                if self.active:
                    self.active = False
        if self.active:
            self.current_color = self.active_text_color
        else:
            self.current_color = self.inactive_text_color
            self.index = 0
            if self.text == "00:00:00":
                self.text = default_time

        if event.type == pg.KEYDOWN:
            if event.key == pg.K_RETURN and not self.active:
                self.text = default_time
                self.active = False
            if self.active:
                if event.key == pg.K_BACKSPACE and self.index > 0:
                    lst = list(self.text)
                    count = len(lst) - 1
                    for _ in range(len(lst) - 1):
                        if lst[count] == ":":
                            count -= 1
                            continue
                        if lst[count - 1] == ":":
                            lst[count] = lst[count - 2]
                        else:
                            lst[count] = lst[count - 1]
                        count -= 1
                    lst[0] = '0'
                    self.index -= 1
                    self.text = ''.join(lst)

                elif self.index < 6:
                    key = event.unicode
                    if key.isdigit():
                        lst = list(self.text)
                        for i in range(len(lst) - 1):
                            if lst[i] == ":":
                                continue
                            if lst[i + 1] == ":":
                                lst[i] = lst[i + 2]
                            else:
                                lst[i] = lst[i + 1]
                        lst[-1] = key
                        self.index += 1
                        self.text = ''.join(lst)

    def draw(self):
        text = font.render(self.text, True, self.current_color)
        self.surface.blit(text, self.text_rect)
        pg.draw.rect(self.surface, self.box_color, self.rect, 3)
        if self.count >= 50:
            self.count = 0
        if self.active and self.draw_cursor:
            if self.count <= 25:
                pg.draw.line(self.surface, black, (self.pos[0] + 166, self.rect.top + 4),
                             (self.pos[0] + 166, self.rect.bottom - 4), 2)
            self.count += 1
        else:
            self.count = 0

    def get_seconds(self):
        lst = list(self.text)
        time = []
        for i, digit in enumerate(lst):
            if digit == ":":
                lst.pop(i)
        skip = False
        for i in range(6):
            if skip:
                skip = False
                continue
            if not skip:
                time.append(lst[i] + lst[i + 1])
                skip = True
        seconds = int(time[0]) * 3600 + int(time[1]) * 60 + int(time[2])
        return seconds


class ShutdownEntry(Entry):
    def __init__(self, w, h, pos, surface):
        super().__init__(w, h, pos, surface)
        self.inactive_text_color = red
        self.active_text_color = red
        self.shutting_down = False
        self.text = default_shutdown_time
        self.shut_down_time = self.get_seconds()
        self.current_time = 0

    def event_handler(self, event):
        if event.type == pg.MOUSEBUTTONDOWN:
            mouse = pg.mouse.get_pos()
            if self.rect.collidepoint(mouse):
                subprocess.call("shutdown -a", shell=True)
                self.active = not self.active
                self.text = "00:00:00"
                self.shutting_down = False
            else:
                if self.active:
                    self.active = False
        if self.active:
            self.current_color = self.active_text_color
        else:
            self.current_color = self.inactive_text_color
            self.index = 0

        if event.type == pg.KEYDOWN:
            if self.active:
                if event.key == pg.K_BACKSPACE and self.index > 0:
                    lst = list(self.text)
                    count = len(lst) - 1
                    for _ in range(len(lst) - 1):
                        if lst[count] == ":":
                            count -= 1
                            continue
                        if lst[count - 1] == ":":
                            lst[count] = lst[count - 2]
                        else:
                            lst[count] = lst[count - 1]
                        count -= 1
                    lst[0] = '0'
                    self.index -= 1
                    self.text = ''.join(lst)

                elif self.index < 6:
                    key = event.unicode
                    if key.isdigit():
                        lst = list(self.text)
                        for i in range(len(lst) - 1):
                            if lst[i] == ":":
                                continue
                            if lst[i + 1] == ":":
                                lst[i] = lst[i + 2]
                            else:
                                lst[i] = lst[i + 1]
                        lst[-1] = key
                        self.index += 1
                        self.text = ''.join(lst)

    def update(self, button):
        self.draw()
        if self.shutting_down and self.get_seconds() > 0:
            self.inactive_text_color = red
            self.current_time = round(time.time())
            m, s = divmod(self.shut_down_time - self.current_time, 60)
            h, m = divmod(m, 60)
            self.text = f"{h:02}:{m:02}:{s:02}"
            button.update_text("Cancel")
        else:
            self.shutting_down = False
            self.inactive_text_color = black
            button.update_text("Shutdown")

    def start(self):
        subprocess.call("shutdown -a", shell=True)
        if self.shutting_down:
            self.shutting_down = False
            self.text = default_shutdown_time
        elif self.get_seconds() != 0:
            self.current_time = round(time.time())
            self.shut_down_time = self.current_time + self.get_seconds()
            self.shutting_down = True
            subprocess.call(f"shutdown -s -f -t {self.get_seconds()}", shell=True)


def alarm(duration):
    if not muted:
        duration *= 1000
        alert.play()
        alert.fadeout(duration)


class TkTimeEntry:
    def __init__(self, frame, width, string_var, fnt):
        self.width = width
        self.font = fnt
        self.sv = string_var
        self.root = frame
        self.last_text = self.sv.get()
        self.index = 0
        self.entry = tk.Entry(self.root, width=self.width, textvariable=self.sv, font=self.font, bg="snow")
        self.key = "0"
        self.entry.bind("<Key>", self.get_key)
        self.entry.bind("<Button-1>", lambda *args: self.on_click())
        self.sv.trace("w", lambda *args: self.callback())

    def on_click(self):
        self.sv.set("00:00:00")
        self.last_text = self.sv.get()
        self.index = 0
        self.root.after_idle(self.entry.icursor, 9)

    def place(self, x, y):
        self.entry.place(x=x, y=y)

    def get_key(self, key):
        self.key = key

    def callback(self):
        if isinstance(self.key, tk.Event):
            self.sv.set(self.last_text)
            if self.key.keysym == "BackSpace" and self.index > 0:
                lst = list(self.last_text)
                count = len(lst) - 1
                for _ in range(len(lst) - 1):
                    if lst[count] == ":":
                        count -= 1
                        continue
                    if lst[count - 1] == ":":
                        lst[count] = lst[count - 2]
                    else:
                        lst[count] = lst[count - 1]
                    count -= 1
                lst[0] = '0'
                self.index -= 1
                self.last_text = ''.join(lst)
                self.sv.set(self.last_text)
            self.key = self.key.char
            if self.index < 6:
                if self.key.isdigit():
                    lst = list(self.last_text)
                    for i in range(len(lst) - 1):
                        if lst[i] == ":":
                            continue
                        if lst[i + 1] == ":":
                            lst[i] = lst[i + 2]
                        else:
                            lst[i] = lst[i + 1]
                    lst[-1] = self.key
                    self.index += 1
                    self.last_text = ''.join(lst)
                    self.sv.set(self.last_text)
            self.root.after_idle(self.entry.icursor, 9)

    def get(self):
        return self.entry.get()


def settings_screen():
    if not os.path.isfile("timer_settings.ini"):
        with open("timer_settings.ini", "w+") as f:
            f.write("[DEFAULTS]\n")
            f.write("default time: 00:14:00\n")
            f.write("default shutdown time: 00:00:00\n")
            f.write("sound: chimes.wav\n")
            f.write("alert0: 4\n")
            f.write("alert1: 16\n")
            f.write("alert2: 8")

    def save():
        confirm = messagebox.askokcancel("Confirm", "Restart program or clear timers for new settings to take effect")
        if confirm:
            with open("timer_settings.ini", "w+") as f:
                f.write("[DEFAULTS]\n")
                if time_entry.get() == "00:00:00":
                    time_entry.sv.set("00:14:00")
                f.write("default time: %s\n" % time_entry.get())
                f.write("default shutdown time: %s \n" % shutdown_entry.get())
                f.write("sound: %s\n" % sound_path.get())
                f.write("alert0: %s\n" % sound_entries_sv[0].get())
                f.write("alert1: %s\n" % sound_entries_sv[1].get())
                f.write("alert2: %s" % sound_entries_sv[2].get())
                root.destroy()

    def open_file(set_default):
        if not set_default:
            root.filename = filedialog.askopenfilename(initialdir="/", title="Select file",
                                                       filetypes=((".wav", "*.wav"), ("all files", "*.*")))
            if root.filename:
                sound_path.set(root.filename.replace('\\', '/'))
                sound_sv.set(os.path.basename(root.filename))
        else:
            sound_path.set(resource_path("chimes.wav"))
            sound_sv.set("chimes.wav")

    def default_sound_times():
        sound_entries_sv[0].set(4)
        sound_entries_sv[1].set(16)
        sound_entries_sv[2].set(8)

    root = tk.Tk()
    root.configure(background='azure')
    root.geometry("300x600")
    root.title("Settings")
    root.iconbitmap(resource_path("icon.ico"))
    root.focus_force()

    time_text = tk.StringVar()
    time_text.set(default_time)
    shutdown_text = tk.StringVar()
    shutdown_text.set(default_shutdown_time)

    shutdown_label = tk.Label(root, text="Default Shutdown Time", font="Helvetica 13 bold", relief=tk.RAISED)
    shutdown_label.place(x=20, y=120)
    shutdown_entry = TkTimeEntry(root, 7, shutdown_text, "BOLD 35")
    shutdown_entry.place(20, 150)

    sound_sv = tk.StringVar()
    sound_path = tk.StringVar()
    sound_path.set(alert_path.replace('\\', '/'))
    sound_sv.set(os.path.basename(sound_path.get()))
    d_time_label = tk.Label(root, text="Default Time", font="Helvetica 13 bold", relief=tk.RAISED)
    d_time_label.place(x=20, y=20)
    d_sound_label = tk.Label(root, text="Alert Sound", font="Helvetica 13 bold", relief=tk.RAISED)
    d_sound_label.place(x=20, y=220)
    d_sound = tk.Label(root, textvariable=sound_sv, font="BOLD 25", relief=tk.GROOVE)
    d_sound.place(x=20, y=250)

    sound_label = tk.Label(root, text="Sound fade-off times \n(0 for no sound)", font="Helvetica 13 bold",
                           relief=tk.RAISED)
    sound_label.place(x=20, y=350)
    sound_entries = []
    sound_entries_sv = [tk.StringVar() for _ in range(3)]
    y = 405
    s_labels = [tk.Label(root, text="Timer alert", font="10", relief=tk.GROOVE),
                tk.Label(root, text="Imperial Delivery warning", font="10", relief=tk.GROOVE),
                tk.Label(root, text="Imperial Delivery notice ", font="10", relief=tk.GROOVE)]
    for i in range(3):
        sound_entries_sv[i].set(timer_durations[i])
        sound_entries.append(tk.Entry(root, width=2, font="BOLD 16", textvariable=sound_entries_sv[i], bg="snow"))
        sound_entries[i].place(x=20, y=y)
        s_labels[i].place(x=60, y=y)
        y += 35
    d_sound__btn = tk.Button(root, text="Set defaults", font="BOLD 10 bold", command=default_sound_times)
    d_sound__btn.place(x=20, y=y)
    time_entry = TkTimeEntry(root, 7, time_text, "BOLD 35")
    button = tk.Button(root, text="Save", width=5, font="BOLD 15", command=save)
    open_btn = tk.Button(root, text="Open", width=5, font="BOLD 10 bold", command=lambda: open_file(False))
    default_btn = tk.Button(root, text="Default", width=6, font="BOLD 10 bold", command=lambda: open_file(True))
    open_btn.place(x=20, y=300)
    default_btn.place(x=90, y=300)
    button.pack(side=tk.BOTTOM)
    time_entry.place(20, 50)

    root.mainloop()


def read_settings():
    global default_time
    global default_shutdown_time
    global alert
    global alert_path
    global timer_durations
    try:
        if os.path.isfile("timer_settings.ini"):
            config = configparser.ConfigParser()
            config.read("timer_settings.ini")
            default_time = config.get("DEFAULTS", "default time")
            if default_time == "00:00:00":
                default_time = "00:14:00"
            default_shutdown_time = config.get("DEFAULTS", "default shutdown time")
            alert_path = config.get("DEFAULTS", "sound")
            alert = pg.mixer.Sound(alert_path)
            timer_durations[0] = int(config.get("DEFAULTS", "alert0"))
            timer_durations[1] = int(config.get("DEFAULTS", "alert1"))
            timer_durations[2] = int(config.get("DEFAULTS", "alert2"))

    except Exception as e:
        print(e)


def about():
    fnt = pg.font.SysFont("Comic Sans MS", 15, True)
    width, height = 390, 120
    end_surface = pg.Surface((width, height), pg.SRCALPHA)
    end_surface.fill((255, 255, 255, 0))
    translucent_surface = pg.Surface((width, height), pg.SRCALPHA)
    translucent_surface.fill((255, 255, 255, 230))
    lines = ["Any questions or problems pm Xain#6873 on discord",
             "-Pressing Enter will make a new timer with", "the default time.",
             "-Clicking on any timer will kill it."]
    txt = []
    y = 0
    for i in range(len(lines)):
        txt.append(fnt.render(lines[i], True, black))
        translucent_surface.blit(txt[i], (0, y))
        y += 30
    mainDisplay.blit(translucent_surface, (display_width - width, 140))


def main_loop():
    global muted
    muted = False
    read_settings()
    start_btn = Button(200, 50, (display_width - 205, display_height - 120), "Start", b_img)
    clear_btn = Button(200, 50, (display_width - 205, display_height - 60), "Clear All", b_img)
    mute_btn = Button(40, 40, (display_width - 140, 85), "", mute_img, True)
    settings_btn = Button(40, 40, (display_width - 95, 85), "", settings_img)
    info_btn = Button(40, 40, (display_width - 45, 85), "", info_img)
    entry = Entry(200, 50, (display_width - 205, display_height - 190), mainDisplay)
    shutdown_entry = ShutdownEntry(200, 50, (display_width - 205, display_height - 330), mainDisplay)
    shutdown_btn = Button(200, 50, (display_width - 205, display_height - 270), "Shutdown", s_b_img)
    timers = []
    imperial_timer = ImperialTimer(display_width - 180, 40)
    while True:
        mainDisplay.fill(white)
        start_btn.draw()
        shutdown_btn.draw()
        clear_btn.draw()
        mute_btn.draw()
        settings_btn.draw()
        info_btn.draw()
        imperial_timer.draw_time()
        entry.draw()
        shutdown_entry.update(shutdown_btn)
        if timers:
            for t in timers:
                t.draw_time()
        for event in pg.event.get():
            entry.event_handler(event)
            shutdown_entry.event_handler(event)
            if event.type == pg.QUIT:
                if shutdown_entry.shutting_down == True:
                    tk.Tk().wm_withdraw()
                    messagebox.showwarning("", "Scheduled shutdown won't be cancelled.")
                sys.exit()
            if start_btn.click(event):
                if start_btn.active:
                    seconds = entry.get_seconds()
                    if len(timers) == 0:
                        timers.append(Timer(seconds))
                    else:
                        if timers[0].dead_timer() or Timer.no_of_timers < 18:
                            timers.append(Timer(seconds))
            if clear_btn.click(event) and clear_btn.active:
                clear_btn.draw()
                pg.display.update()
                pg.time.wait(150)
                for i, t in enumerate(timers):
                    t.dead = True
                    Timer.no_of_timers = 0
                    Timer.instances = []
                    timers.pop(i)
                if shutdown_entry.shutting_down:
                    subprocess.call("shutdown -a", shell=True)
                main_loop()
            if mute_btn.click(event):
                if mute_btn.active:
                    muted = True
                else:
                    muted = False
            if settings_btn.click(event):
                if settings_btn.active:
                    settings_btn.draw()
                    pg.display.update()
                    pg.time.wait(100)
                    _thread.start_new_thread(settings_screen, ())
            if shutdown_btn.click(event):
                if shutdown_btn.active:
                    shutdown_entry.start()
            if event.type == pg.KEYDOWN:
                if event.key == pg.K_RETURN:
                    seconds = entry.get_seconds()
                    if len(timers) == 0:
                        timers.append(Timer(seconds))
                    else:
                        if timers[0].dead_timer() or Timer.no_of_timers < 18:
                            timers.append(Timer(seconds))
                    entry.active = False

            if event.type == pg.MOUSEBUTTONDOWN:
                for t in timers:
                    t.clicked()
        if info_btn.hover():
            about()
        clock.tick(60)
        pg.display.update()


main_loop()
